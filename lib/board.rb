class Board
  attr_reader :grid

  def initialize(grid = nil)
    @grid = grid || Array.new(3) {Array.new(3)}
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos,val)
    x,y = pos
    @grid[x][y] = val
  end

  def empty?(pos)
    self[pos].nil?
  end

  def place_mark(pos,mark)
    self[pos] = mark
  end

  def winner
    [:X, :O].each {|mark| return mark if won?(mark)}
    nil
  end

  def over?
    return true if winner
    tied_condition = @grid.all? do |line|
      line.all? {|cell| cell != nil}
    end

    return true if tied_condition
    false
  end

  def to_s
    @grid.map do |row|
      row.map {|el| el.nil? ? "-" : el}.join('   ')
    end.join("\n")
  end

  def valid?(move)
    move.all? { |coord| coord.between?(0, 2) } &&
    empty_positions.include?(move)
  end

  def empty_positions
    positions.select {|pos| self.empty?(pos)}
  end

  def positions
    p = []
    3.times do |row|
      3.times {|col| p << [row, col] }
    end
    p
  end


  private

  def won?(mark)
    lines.any? do |line|
      line.all? {|pos| self[pos] == mark}
    end
  end

  def lines

    h_lines = (0..2).map do |h|
      [[h, 0], [h, 1], [h, 2]]
    end

    v_lines = h_lines.transpose


    d_lines = [
      [[0,0], [1,1], [2,2]],
      [[2,0], [1,1], [0,2]]
    ]

    h_lines + v_lines + d_lines
  end
end


if __FILE__ == $PROGRAM_NAME
  b = Board.new
  puts b.to_s
  puts "----------------"
  b.place_mark([0,1], :X)
  b.place_mark([2,0], :O)
  puts b.to_s
end
