class HumanPlayer
attr_reader :name
attr_accessor :mark
  def initialize(name)
    @name = name
    @mark = mark
  end

  def get_move
    puts "Please, make a move! Ex. '0, 0' "
    move = gets.chomp

    until valid_move?(move)
      print "Please, make a valid move! Ex. '0, 0' "
      move = gets.chomp
    end
    parse_move(move)
  end

  def valid_move?(move)
    move = parse_move(move)
    return false unless move.length == 2
    @board.valid?(move)
  end

  def parse_move(move)
    move.split(',').map(&:to_i)
  end

  def display(board)
    @board = board
    puts board.to_s
    puts
  end
end
